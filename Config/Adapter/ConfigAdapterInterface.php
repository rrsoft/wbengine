<?php

/**
 *
 * @author roza
 */

namespace Wbengine\Config\Adapter;

interface ConfigAdapterInterface {


    public function getDbCredentials();

    public function getHtmlHeaderCharset();

    public function getCssCollection();

    public function getAdminIpCollection();

    public function getIsDebugEnabled();

    public function getTimeZone();

    public function toArray();
}
