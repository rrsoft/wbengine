<?php

/**
 * $Id: Site.php 85 2010-07-07 17:42:43Z bajt $
 * ----------------------------------------------
 * Abstarct Class for the config adapters
 *
 * @package RRsoft-CMS * @version $Rev: 30 $
 * @copyright (c) 2009-2010 RRsoft www.rrsoft.cz
 * @license GNU Public License
 *
 * Minimum Requirement: PHP 5.1.x
 */

namespace Wbengine\Config\Adapter;

//use Wbengine\Config\ConfigAdapterInterface;

abstract class AdapterAbstract implements ConfigAdapterInterface {


    public function getDbCredentials()
    {
	return $this->dbAdapterDefinition;
    }


    public function getHtmlHeaderCharset()
    {
	return $this->codePage;
    }


    public function getCssCollection()
    {
	return $this->cssFiles;
    }


    public function getAdminIpCollection()
    {
	return $this->ipAdmins;
    }


    public function getIsDebugEnabled()
    {
	return $this->debug;
    }


    public function getTimeZone()
    {
	return $this->timeZone;
    }

}
