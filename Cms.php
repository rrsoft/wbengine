<?php


use Wbengine\User;

/**
 * $Id: Site.php 85 2010-07-07 17:42:43Z bajt $
 * ----------------------------------------------
 * CMS class provide most of general
 * functionalities as store data to session,
 * manage user's accounts, site locales and etc.
 *
 * @package RRsoft-CMS * @version $Rev: 30 $
 * @copyright (c) 2009-2010 RRsoft www.rrsoft.cz
 * @license GNU Public License
 *
 * Minimum Requirement: PHP 5.1.x
 */
final class Class_Cms {


    /**
     * Locale class
     * @var Class_Locale
     */
    private $_locale = NULL;

    /**
     * Loaded user's data
     * @var array
     */
    private $_userData = NULL;

    /**
     * Instance of user class
     * @var Class_User
     */
    private $_classUser = NULL;

    /**
     * Created session object
     * @var Class_Session
     */
    private $_session = NULL;

    /**
     * Just debugging trigger
     * @var booolean
     */
    private $_debug = true;

    /**
     * Instance of Class_Config
     * @var Class_Config
     */
    public $config = NULL;

    /**
     * Instance of Class_Renderer
     * @var Class_Renderer
     */
    private $_renderer = NULL;

    /**
     * Error handler
     * @var Class_Error_Handler
     */
    private $_errorHandler = NULL;

    /**
     * Instance of Class_Site_Url
     * @var Class_Site_Url
     */
    private $_classUrl = NULL;

    /**
     * CMS member object exception
     * @var Exception
     */
    private $_exception = NULL;

    /**
     * Instance of object Class_Site
     * @var type Class_Site
     */
    private $_site = NULL;

    /**
     * Site Vars
     * @var Class_Vars
     */
    private $_classVars = NULL;

    /**
     * URL redirection array
     * @var type array
     */
    private $_redirections = array();



    /**
     * In constructor we parse URL and create all
     * needed variables.
     */
    public function __construct(array $configs)
    {
	if (isset($configs["config"]) && $configs["config"] instanceof Class_Config) {
	    $_config = $configs["config"];
	} else {
	    include_once 'Class/Cms/SessionException.php';
	    throw new Class_Cms_Exception("Config paramets should be and instance of Class_Config,", 500);
	}

	$this->setDebug((boolean) $configs["debug"]);
	$this->_setup($_config);
    }



    /**
     * Set Config adapter for latest use.
     * @param Class_Config $config
     */
    private function _setup(Class_Config $config)
    {
	try {
	    include_once 'Class/Cms/SessionException.php';
	    include_once 'Zend/Db.php';
	    include_once 'Zend/Registry.php';

	    // store config adapter for latest use
	    $this->config = $config->getAdapter();

	    // set default time zone...
	    date_default_timezone_set($this->config->getDefaultTimeZone());

	    // Set default error handler...
	    set_error_handler(array($this->getErrorHandler(), 'SetErrorHandler'));

	    // prepare PDO connection...
	    $db = Zend_Db::factory($this->config->getDbAdapterName(), $this->config->getDbCredentials());
	    Zend_Registry::set('db', $db);

	    $this->_db = $db->getConnection();
	}
	catch (Zend_Db_Adapter_Exception $e) {
	    throw new Zend_Db_Adapter_Exception($e->getMessage(), $e->getCode());
	}
	catch (Zend_Exception $e) {
	    throw new Zend_Exception($e->getMessage(), $e->getCode());
	}
    }



    /**
     * Return all defined redirections.
     * @return array
     */
    public function getRedirections()
    {
	return $this->_redirections;
    }



    /**
     * Set parameter for debuging
     * @param boolean $value
     */
    public function setDebug($value = FALSE)
    {
	$this->_debug = $value;
    }



    /**
     * Get debuging trigger
     * @param boolean $debug
     */
    public function isDebugOn()
    {
	return $this->_debug;
    }



    /**
     * Add given value to local vars.
     * In order you can specify parent key.
     *
     * @param string $key
     * @param mixed $value
     * @param string $parentKey
     */
    public function setValue($key, $value = NULL, $parentKey = NULL)
    {
	if (!empty($parentKey)) {
	    $this->getClassVars()->addValue($key, $value, $parentKey);
	} else {
	    $this->getClassVars()->addValue($key, $value);
	}
    }



    /**
     * Return all assigned site variables.
     * @return array
     */
    public function getVars()
    {
	return $this->getClassVars()->getValues();
    }



    /**
     * Return instance of Class_Vars
     * @return Class_Vars
     */
    public function getClassVars()
    {
	if (!$this->_classVars instanceof Class_Vars) {
	    $this->_setClassVars();
	}

	return $this->_classVars;
    }



    /**
     * Create instance of Class_Cms_Vars
     * @void
     */
    private function _setClassVars()
    {
	require_once 'Class/Vars.php';
	$this->_classVars = new Class_Vars($this);
    }



    /**
     * Return CMS member object Exception
     * @return Exception
     */
    public function getException()
    {
	return $this->_exception;
    }



    /**
     * Return instance of class User.
     * @return User
     */
    public function getClassUser()
    {
	if (!$this->_classUser instanceof User) {
	    $this->_classUser = new User($this);
	}

	return $this->_classUser;
    }



    /**
     * Return user's data loaded for an session.
     * @return array
     */
    public function getIdentity()
    {
	if (is_array($this->_userData) && sizeof($this->_userData)) {
	    return $this->_userData;
	} else {
	    $this->_setIdentity();
	}

	return $this->_userData;
    }



    /**
     * Set user data to local variable for latest use.
     * @param array $userData
     */
    private function _setUserData($userData)
    {
	$this->_userData = $userData;
    }



    /**
     * Set user identities if needed.
     * @see getIdentity
     */
    private function _setIdentity()
    {
	$this->_setUserData($this->getClassUser()->getIdentity());
    }



    /**
     * Store locale class.
     * @param Class_Locale_Abstract $locale
     */
    private function _setLocale(Class_Locale_Abstract $locale)
    {
	$this->_locale = $locale;
    }



    /**
     * Return created session instance.
     * @return Class_Session
     */
    public function getSession()
    {
	if (NULL === $this->_session) {
	    $this->_setSession();
	    // We call init methodt with TRUE argument to
	    // clean session database and clear all old records.
	    $this->_session->init(TRUE);
	}

	return $this->_session;
    }



    /**
     * Return a created locale instance.
     * @return Class_Locale
     */
    public function getLocale()
    {
	if (NULL === $this->_locale) {
	    $this->_setLocale($this->getSession()->getLocale());
	}

	return $this->_locale;
    }



    /**
     * Return a config class object
     * @return Class_Config
     */
    public function getConfig()
    {
	return $this->config;
    }



    /**
     * Return created object renderer
     * @return Class_Renderer
     */
    public function getRenderer()
    {
	If (NULL === $this->_renderer) {
	    $this->_setRenderer();
	}

	return $this->_renderer;
    }



    /**
     * Create New CMS object member Exception
     * @param string $message
     * @param integer $code
     */
    public function addException($message, $code = null)
    {
	include_once 'Class/Cms/SessionException.php';
	$this->_exception = new Class_Cms_Exception($message, $code);
    }



    /**
     * Return instance of Class_Url
     * @return Class_Url
     */
    public function getClassUrl()
    {
	if (NULL === $this->_classUrl) {
	    require_once 'Class/Site/Url.php';
	    $this->_classUrl = new Class_Site_Url($this);
	}

	return $this->_classUrl;
    }



    /**
     * Return instance of Class_Error_Handler
     * @return Class_Error_Handler
     */
    public function getErrorHandler()
    {
	if (NULL === $this->_errorHandler) {
	    include_once 'Class/Error/Handler.php';
	    $this->_errorHandler = New Class_Error_Handler();
	}

	return $this->_errorHandler;
    }



    /**
     * Create object Class_Renderer
     */
    private function _setRenderer()
    {
	include 'Class/Renderer.php';
	$this->_renderer = new Class_Renderer($this);
    }



    /**
     * Create new session instance object if needed.
     * @see Class_Session
     */
    private function _setSession()
    {
//	include 'Class/Session.php';
	$this->_session = new Class_Session();

	if (!$this->_session instanceof Class_Session_Abstract) {
	    $this->_addException('Object is not an instance of Class_Session_Abstract', Class_Site_Exception::ERROR_NOT_INSTANCE_OF_SESSION);
	}
    }



    private function _getSite()
    {
	if ($this->_site instanceof Class_Site) {
	    return $this->_site;
	} else {
	    include_once 'Class/Url.php';
	    $this->_site = new Class_Site($this);
	}
	return $this->_site;
    }



    public function getSite()
    {
	return $this->_getSite();
    }



    public function show()
    {
	$site = $this->getSite()->init();

	if (FALSE === $site) {
	    $this->addException('Site not found.', HTML_ERROR_404);

	    $this->setValue(HTML_CENTRAL_SECTION, $this->getRenderer()->getErrorBox($this->getException()));
//	    var_dump($this->getException());
	    // as additional option we allways render
	    // also footer box when error has fired
//            $this->setValue(HTML_FOOTER_SECTION,
//		$this->getRenderer()->render("footer_box"));
	}

//        var_dump($site);
//        $_header = "'Content-type: text/html; charset=".$this->config->getDefaultCodePage()."'";
	header("'Content-type: text/html; charset=" . $this->config->getHtmlHeaderCharset() . "'");
	$this->getRenderer()->dispatch($this);
    }



}
