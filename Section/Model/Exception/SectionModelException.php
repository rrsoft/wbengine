<?php

/**
 * Description of Class_Session_Exception
 *
 * @author bajt
 */
/**
 * @see Class_Exception
 */

namespace Wbengine\Section\Model\Exception;

use Wbengine\Section\Exception\SectionException;

class SectionModelException extends SectionException {

}
