<?php

/**
 * @see Class_Site_Exception
 */

namespace Wbengine\Vars;

use Wbengine\Exception\RuntimeException;

class VarsException extends RuntimeException {

}
