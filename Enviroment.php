<?php

/**
 * Description of Enviroment
 *
 * @author bajt
 */

namespace Wbengine;

//die(dirname(__DIR__) . "/Wbengine/Etc/Seo.inc");


class Enviroment {


    /**
     * Static class - cannot be instantiated.
     */
    final public function __construct()
    {
	require_once 'Class/SessionException.php';
	throw new Wbengine_Class_Exception("Cannot instantiate static class " . get_class($this));
    }


    /**
     * return User's agent.
     * @return string
     */
    static function getUserAgent()
    {
	return $_SERVER['HTTP_USER_AGENT'];
    }


    /**
     * Return Default hostname include protocol
     * @param type $protocol
     * @return string
     */
    static function getHost( $protocol = NULL )
    {
	return (empty($protocol)) ? "http://" . $_SERVER['HTTP_HOST'] : $protocol . "://" . $_SERVER['HTTP_HOST'];
    }


    /**
     * Return user's IP.
     * @return string
     */
    static function getUserIp()
    {
	if ( isset($_SERVER["REMOTE_ADDR"]) ) {
	    return $_SERVER["REMOTE_ADDR"];
	} elseif ( isset($_SERVER["HTTP_X_FORWARDED_FOR"]) ) {
	    return $_SERVER["HTTP_X_FORWARDED_FOR"];
	} elseif ( isset($_SERVER["HTTP_CLIENT_IP"]) ) {
	    return $_SERVER["HTTP_CLIENT_IP"];
	}
    }


    /**
     * Check for valid email address.
     *
     * @param string $email
     * @return boolean
     */
    static function checkValidEmail( $email )
    {
	if ( preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email) ) {
	    list($username, $domain) = explode('@', $email);
	    if ( !checkdnsrr($domain, 'MX') ) {
		return false;
	    }
	    return true;
	}

	return false;
    }


    /**
     * Create URL from given string related to url.
     * @param string $url
     * @param string $html
     * @return string
     */
    static function createUrl( $url, $html = false )
    {
	return $tmp = self::createSeo($url) . (($html) ? '.html' : '/');
//	return ( substr($tmp, 0, 0) === '/' ) ? '' : '/' . $tmp;
    }


    /**
     * Create SEO relevant name from given string.
     * @param string $string
     * @return string
     */
    static function createSeo( $string = NULL )
    {
	require_once 'Etc/Seo.inc';
//	var_dump(function_exists('create_filename_from_text'));
//	die();
	if ( function_exists('create_filename_from_text') ) {
	    return create_filename_from_text($string);
//	    die(create_filename_from_text($string));
	} else {
	    return $string;
	}
    }


    static function emailSend( $message, $subject, $email )
    {
	$tmp = "Formular conten't:\n----------------------------------\n";
	foreach ( $_POST as $key => &$value ) {
	    if ( strtoupper($key) == "MESSAGE" )
		$tmp .= "\n\n";

	    $tmp .= "$key: $value.\n";
	}

	$tmp .= "\n\n\nForm was sent from IP:" . $_SERVER['REMOTE_ADDR'] . " at " . date("Y-m-d H:i:s");

	// Create transport
	$config = array(
	    'ssl' => 'tls',
	    'port' => 587,
	    'auth' => 'login',
	    'username' => 'bajtlamer@gmail.com',
	    'password' => 'bajtfaskoadmin'
	);


	include_once 'Zend/Mail/Transport/Smtp.php';
	include_once 'Zend/Mail.php';
	$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);

	Zend_Mail::setDefaultFrom('form@menimepodnikani.cz', 'Web Form');

	try {
	    $mail = new Zend_Mail('UTF-8');
	    $mail->addTo($email);

	    $mail->setSubject($subject);

//		$mail->setBodyText($message);
	    $mail->setBodyHtml($message);

	    $mail->send($transport);
	} catch (Exception $e) {
	    return $e->getMessage();
	}

	// Reset defaults
	Zend_Mail::clearDefaultFrom();
	Zend_Mail::clearDefaultReplyTo();
	return true;
    }

}
