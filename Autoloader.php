<?php
return;
if ( !defined('ROBOX_DIR') ) {
    define('ROBOX_DIR', __DIR__ . '');
}


$zf2Path = false;
define('ZEND_DIR', __DIR__ . '/Zend');
//var_dump(__DIR__);

if ( is_dir(ZEND_DIR) ) {
    $zf2Path = ZEND_DIR . '/Loader';
} else {
    throw new Exception('ZF2 library directory not found on path: ' . ZEND_DIR);
}

if ( file_exists($zf2Path . '/StandardAutoloader.php') ) {
    require_once $zf2Path . '/StandardAutoloader.php';
} else {
    throw new Exception(__FILE__ . ': ZF2 loader not found: ' . $zf2Path . '/StandardAutoloader.php');
}
//var_dump(dirname(__DIR__) . '/RRsoft/Wbengine');
//var_dump(APP_DIR);
$loader = new \Zend\Loader\StandardAutoloader(array(
    'autoregister_zf' => true,
    'namespaces' => array(
	'App' => dirname(__DIR__) . APP_DIR . '',
    ),
    'namespaces' => array(
	'Wbengine' => __DIR__ ,
    ),
//    'prefixes' => array(
//	'Wbengine' => dirname(__DIR__) . '',
//    ),
    'fallback_autoloader' => true,
	));

//return $loader;
//if (file_exists($autoloaderFile)) {
//    $loader = include $autoloaderFile;
if ( $loader instanceof Zend\Loader\StandardAutoloader ) {
    $loader->register();
} else {
    throw new Exception('Given object is not instance of Zend\Loader\StandardAutoloader!');
}
//} else {
//    throw new Exception('Autoloader file ' . $autoloaderFile . ' not found!');
//}
