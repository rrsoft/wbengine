<?php

/**
 * $Id: Site.php 85 2010-07-07 17:42:43Z bajt $
 * ----------------------------------------------
 * CMS class provide most of general
 * functionalities as store data to session,
 * manage user's accounts, site locales and etc.
 *
 * @package RRsoft-CMS * @version $Rev: 30 $
 * @copyright (c) 2009-2010 RRsoft www.rrsoft.cz
 * @license GNU Public License
 *
 * Minimum Requirement: PHP 5.1.x
 */

namespace Wbengine\Application;

use Wbengine\Error;
use Wbengine\Locale\LocaleAbstract;
use Wbengine\Section;
use Wbengine\Site;
use Wbengine\Renderer;
use Wbengine\Session;
use Wbengine\User;
use Wbengine\Vars;
use Zend\Loader\StandardAutoloader;

require_once dirname(__FILE__) . '/../Enviroments.php';

abstract Class Application
{


    /**
     * Locale class
     * @var Class_Locale
     */
    private $_locale = NULL;

    /**
     * Loaded user's data
     * @var array
     */
    private $_userData = NULL;

    /**
     * Instance of user class
     * @var Class_User
     */
    private $_classUser = NULL;

    /**
     * Created session object
     * @var Class_Session
     */
    private $_session = NULL;

    /**
     * Just debugging trigger
     * @var boolean
     */
    private $debug = FALSE;

    /**
     * Instance of Class_Config
     * @var \Wbengine\Config
     */
    private $config = NULL;

    /**
     * Instance of Class_Renderer
     * @var Class_Renderer
     */
    private $_renderer = NULL;

    /**
     * Error handler
     * @var Class_Error_Handler
     */
    private $errorHandler = NULL;

    /**
     * Instance of Class_Site_Url
     * @var Class_Site_Url
     */
    private $_classUrl = NULL;

    /**
     * CMS member object exception
     * @var Exception
     */
    private $_exception = NULL;

    /**
     * Instance of object Class_Site
     * @var type Class_Site
     */
    private $_site = NULL;

    /**
     * Site Vars
     * @var Class_Vars
     */
    private $_classVars = NULL;

    /**
     * URL redirection array
     * @var type array
     */
    private $_redirections = array();

    /**
     * Zend loader instance
     * @var Zend\Loader\StandardAutoloader
     */
    private $loader = NULL;

    private $dbConnection = NULL;


    /**
     * Return all defined redirections.
     * @return array
     */
    public function getRedirections()
    {
        return $this->_redirections;
    }


    /**
     * Get debuging trigger
     * @internal param bool $debug
     * @return \Wbengine\Application\boolean
     */
    public function isDebugOn()
    {
        return $this->debug;
    }


    /**
     * Add given value to local vars.
     * In order you can specify parent key.
     *
     * @param string $key
     * @param mixed $value
     * @param string $parentKey
     */
    public function setValue($key, $value = NULL, $parentKey = NULL)
    {
        if (!empty($parentKey)) {
            $this->getClassVars()->addValue($key, $value, $parentKey);
        } else {
            $this->getClassVars()->addValue($key, $value);
        }
    }


    /**
     * Return all assigned site variables.
     * @return array
     */
    public function getVars()
    {
        return $this->getClassVars()->getValues();
    }


    /**
     * Return instance of Class_Vars
     * @return Class_Vars
     */
    public function getClassVars()
    {
        if ($this->_classVars instanceof Vars) {
            return $this->_classVars;
        }

        $this->_setClassVars();
        return $this->_classVars;
    }


    /**
     * Create instance of Class_Cms_Vars
     * @void
     */
    private function _setClassVars()
    {
        $this->_classVars = New Vars($this);
    }


    /**
     * Return CMS member object Exception
     * @return Exception
     */
    public function getException()
    {
        return $this->_exception;
    }


    /**
     * Return instance of class User.
     * @return \Wbengine\User
     */
    public function getClassUser()
    {
        if (!$this->_classUser instanceof User) {

            $this->_classUser = new User($this);
        }

        return $this->_classUser;
    }


    /**
     * Return user's data loaded for an session.
     * @return array
     */
    public function getIdentity()
    {
        if (is_array($this->_userData) && sizeof($this->_userData)) {
            return $this->_userData;
        } else {
            $this->_setIdentity();
        }

        return $this->_userData;
    }


    /**
     * Set user data to local variable for latest use.
     * @param array $userData
     */
    private function _setUserData($userData)
    {
        $this->_userData = $userData;
    }


    /**
     * Set user identities if needed.
     * @see getIdentity
     */
    private function _setIdentity()
    {
        $this->_setUserData($this->getClassUser()->getIdentity());
    }


    /**
     * Store locale class.
     * @param \Wbengine\Locale\LocaleAbstract $locale
     */
    private function _setLocale(LocaleAbstract $locale)
    {
        $this->_locale = $locale;
    }


    /**
     * Return created session instance.
     * @return \Wbengine\Session
     */
    public function getSession()
    {
        if (NULL === $this->_session) {
            $this->_setSession();

            $this->_session->init(TRUE);
        }

        return $this->_session;
    }


    /**
     * Return a created locale instance.
     * @return \Wbengine\Locale\LocaleAbstract
     */
    public function getLocale()
    {
        if (NULL === $this->_locale) {
            $this->_setLocale($this->getSession()->getLocale());
        }

        return $this->_locale;
    }


    /**
     * Return a config class object
     * @return Class_Config
     */
    public function getConfig()
    {
        return $this->config;
    }


    /**
     * Set Config adapter
     * @param Wbengine\Config $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }


    /**
     * Return created object renderer
     * @return \Wbengine\Renderer
     */
    public function getRenderer()
    {
        If (NULL === $this->_renderer) {
            $this->_setRenderer();
        }

        return $this->_renderer;
    }


    /**
     * Create New CMS object member Exception
     * @param string $message
     * @param integer $code
     */
    public function addException($message, $code = null)
    {
        $this->_exception = new ApplicationException($message, $code);
    }


    /**
     * Return instance of Class_Url
     * @return Class_Url
     */
    public function getClassUrl()
    {
        if (NULL === $this->_classUrl) {
            require_once 'Class/Site/Url.php';
            $this->_classUrl = new Class_Site_Url($this);
        }

        return $this->_classUrl;
    }


    /**
     * Return instance of Class_Error_Handler
     * @return Class_Error_Handler
     */
    public function getErrorHandler()
    {
        if (NULL === $this->errorHandler) {
            $this->errorHandler = New Error\Handler();
        }

        return $this->errorHandler;
    }

    public function getSectionById($id) {
        $section = New Section($this->getSite());
        return $section->getSection($id);
    }


    /**
     * Create object Class_Renderer
     */
    private function _setRenderer()
    {
        $this->_renderer = New Renderer($this);
    }


    /**
     * Create new session instance object if needed.
     * @see Class_Session
     */
    private function _setSession()
    {
        $this->_session = new Session();

        if (!$this->_session instanceof Session\SessionAbstract) {
            throw New Exception\RuntimeException(__METHOD__
                . ': given object is not the instance of Wbengine\Session\SessionAbstract.');
        }
    }



    /**
     * Create/Return instance of Site object
     * @return \Wbengine\Site
     */
    private function _getSite()
    {
        if ($this->_site instanceof Site) {
            return $this->_site;
        } else {
            $this->_site = new Site($this);
        }
        return $this->_site;
    }


    /**
     * Return site object instance
     * @return \Wbengine\Site
     */
    public function getSite()
    {
        return $this->_getSite();
    }


    /**
     * Create/return instance of Zend loader
     * @return Zend\Loader\StandardAutoloader
     */
    private function getLoader()
    {
        if ($this->loader === NULL) {
            require_once 'Vendor/Zend/Loader/StandardAutoloader.php';
            $this->loader = new StandardAutoloader(array(
                'autoregister_zf' => true,
                'namespaces' => array(
                    'Wbengine' => dirname(__DIR__) . '/Wbengine',
                ),
                'fallback_autoloader' => true,
            ));
        }

        return $this->loader;
    }


    public function getDbAdapter()
    {
        return $this->dbConnection;
    }


    public function run($handler=null)
    {
        $site = $this->getSite()->init();

        if ($handler===HTML_ERROR_410) {
            $this->addException('Gone.', HTML_ERROR_410);
            $this->setValue(HTML_CENTRAL_SECTION, $this->getRenderer()->getErrorBox($this->getException()));
        }

        if (FALSE === $site->isLoaded()) {
            $this->addException('Site not found.', HTML_ERROR_404);
            $this->setValue(HTML_HEADER_SECTION, $this->getRenderer()->render("Central/slider"));
            $this->setValue(HTML_CENTRAL_SECTION, $this->getRenderer()->getErrorBox($this->getException()));
        }

        $this->getRenderer()->dispatch($this);
    }


}
