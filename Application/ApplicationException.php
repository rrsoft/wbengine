<?php

/**
 * Exception
 */

namespace Wbengine\Application;

use Wbengine\Exception\RuntimeException;

class ApplicationException extends RuntimeException {

}
