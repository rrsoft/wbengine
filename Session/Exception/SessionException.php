<?php

namespace Wbengine\Session\Exception;
use Wbengine\Exception\RuntimeException;


/**
 * Class SessionException
 */
class SessionException extends RuntimeException {}
