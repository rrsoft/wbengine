<?php

namespace Wbengine\Renderer\Exception;

use Wbengine\Exception\RuntimeException;

class RendererException extends RuntimeException {

}
