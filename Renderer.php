<?php

/**
 * $Id$
 * ----------------------------------------------
 * This renderer class manage and render all HTML
 * templates used in CMS.
 * Class use default HTML template system defined
 * in site class.
 *
 * @package RRsoft-CMS
 * @version $Rev$
 * @copyright (c) 2009-2010 RRsoft www.rrsoft.cz
 * @license GNU Public License
 *
 * Minimum Requirement: PHP 5.1.x
 */

namespace Wbengine;

use Wbengine\Exception\RuntimeException;

//use Wbengine\Renderer;

class Renderer extends Renderer\Adapter
{

    /**
     * Default tamplates files extensions
     * @var string
     */
    private $_extension = '.tpl';


    /**
     * Given CMS class object
     * @var object
     */
    private $_cms = NULL;


    /**
     * Given site class object
     * @var object
     */
    private $_site = NULL;


    /**
     * HTML formater
     * @var object
     */
    private $_formater = NULL;


    /**
     * Default renderer class anme
     * @var string
     */
    private $_rendererName = 'Smarty';


    /**
     * Default formater class name
     * @var string
     */
    private $_formaterName = 'texy';


    /**
     * Default template path
     * @var string
     */
    private $_templatePath = 'App/Src/View/Front/';
    private $_formaterPath = 'vendor/Texy/';


    /**
     * Create HTML templater object and
     * assign all defined variables.
     *
     * @param object $webengine
     */
    function __construct(\Wbengine\Application\Application $webengine)
    {
        if ($webengine instanceof \Wbengine\Application\Application) {
            $this->_cms = $webengine;
            $this->_site = $webengine->getSite();
        } else {
            throw new Exception\RuntimeException('Require object Class_site, but NULL given.');
        }

        $this->setAdapterName($this->_rendererName);
        $this->setCompileDir(APP_DIR . '/Cache/Renderer/');
        $this->setTemplateDir(APP_DIR . '/Src/View/Front/');
        $this->setConfigDir(APP_DIR . 'Config/');
    }


    /**
     * Return site instance object
     * @return Class_Site
     */
    public function getCmsObject()
    {
        return $this->_cms;
    }


    /**
     * Return site instance object
     * @return Class_Site
     */
    public function getSite()
    {
        return $this->_site;
    }


    /**
     * Return default teplate extension
     * @return string
     */
    public function getExtension()
    {
        return $this->_extension;
    }


    /**
     * Return declared HTML formater
     * @return object
     */
    public function getFormater()
    {
        if (!$this->_formater) {
            $_formater = new Formater();

            $this->_formater = $_formater->getFormater($this->_formaterName, $this->_formaterPath);
        }

        return $this->_formater;
    }


    /**
     * Return rendered main site template.
     * @return string as HTML content
     */
    public function dispatch(\Wbengine\Application\Application $webengine)
    {
//	var_dump($webengine->getVars());
        try {
            if ($webengine instanceof \Wbengine\Application\Application) {
                // assign all needed vars to templater..
                $this->assign($webengine->getVars(), NULL, 'global');
//            var_dump($webengine->getVars());
                // ...and show content...
//                $this->display('site' . $this->getExtension());
                //@todo JUST TESTING TO MINIFI HTML...
                $source = preg_replace("'\s+'ms", " ", $this->fetch('site' . $this->getExtension()));
                echo($source);
            } else {
                throw New Exception\RuntimeException(__METHOD__
                    . ': Method Excepts Wbengine\Application\Application argument.');
            }
        } catch (Exception\RuntimeException $e) {

            throw New RuntimeException(__METHOD__
                . ": Throwed Exception by object: " . $e->getMessage());
        }
    }


    /**
     * Return rendered compiled template
     * without display them...
     * @return string as HTML content
     */
    public function compile(Class_Cms $cms)
    {
        if ($cms instanceof Class_Cms) {
            // assign all needed vars to templater..
            $this->assign($cms->getVars());
            // ...and show content...
            return $this->fetch('site' . $this->getExtension(), null, null);
        } else {
            include_once 'Class/SessionException.php';
            throw new Wbengine_Class_Exception('Object is not instance of object Class_Cms.');
        }
    }


    /**
     * This method return HTML template content by
     * given template name and with variables by
     * given var array.
     * Also we can choice a type of action.
     * Alowed action is display|fetch.
     *
     * @param string $template
     * @param mixed $vars
     * @throws RuntimeException
     * @return string as HTML
     */
    public function render($template = NULL, $vars = NULL)
    {
        $_path = $this->_templatePath . $template . $this->getExtension();

        if (NULL === $template) {
            throw New Exception\RuntimeException(__METHOD__
                . ': Template name string expected, but null given.');
        }


        if (!file_exists($this->_templatePath . $template . $this->getExtension())) {
            throw New Exception\RuntimeException(__METHOD__
                . ': Template file "' . $_path . '" not exist.');
        }

        // Assign given vars ..?
        if (!empty($vars)) {
            // Remove slashes from the given path...
            $valueName = preg_replace('/^(.*)(\/)(\w+)/i', '$3', $template);
            $this->assign($valueName, $vars);
        }

        if (is_null($template)) {
            return $this->fetch('index.tpl');
        } else {
            return $this->fetch($template . $this->_extension);
        }
    }


    /**
     * Assign variabel to template.
     *
     * @param string $name
     * @param mixed $value
     * @throws Class_Renderer_Exception
     */
    public function setVar($name, $value)
    {
        if (empty($name)) {
            include_once 'Class/Renderer/SessionException.php';
            throw new Class_Renderer_Exception('The var name should be an string.');
        }

        $this->assign($name, $value);
    }


    /**
     * Return html error box created from given
     * exception object.
     * Method also fire HTML header with appropriate
     * html error related to error number taken from
     * the given exception.
     *
     * @param \Wbengine\Exception|\Wbengine\Exception\RuntimeException $exception
     * @return string as HTML
     */
    public function getErrorBox(RuntimeException $exception = NULL)
    {
        if ((int)$exception->getCode() === 0)
            return;

        $error['code'] = $exception->getCode();
        $error['title'] = 'Cauth exception:';

        if ($this->getCmsObject()->isDebugOn()) {
            $error['msg'] = $exception->__toString();
        } else {
            $error['msg'] = $exception->getMessage();
        }

        // Here we can manage templates for an specify errors.
        switch ((int)$exception->getCode()) {
            case HTML_ERROR_404:
                header("HTTP/1.1 404 Not Found");

                $this->getSite()->setHtmlTitle('HTTP/1.1 404 Not Found');
                $tmp = $this->render(HTML_ERROR_404);
                break;

            case HTML_ERROR_401:
                header("HTTP/1.1 401 Unauthorized");

                $this->getSite()->setHtmlTitle('HTTP/1.1 401 Unauthorized');
                $tmp = $this->render(HTML_ERROR_401);
                break;

            case HTML_ERROR_410:
                $this->getSite()->setHtmlTitle('HTTP/1.1 410 Gone');
                $tmp = $this->render(HTML_ERROR_410);
                break;

            default:
                header("HTTP/1.1 500 Internal Server Error");
                $this->getSite()->setHtmlTitle('HTTP/1.1 500 Internal Server Error');
                $tmp = $this->render(HTML_ERROR, $error);
                break;
        }

        return $tmp;
    }

}
