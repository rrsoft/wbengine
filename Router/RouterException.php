<?php

 /**
 * @see Class_Exception
 */

namespace Wbengine\Router;

use Wbengine\Exception\RuntimeException;

class RouterException extends RuntimeException
{
}
