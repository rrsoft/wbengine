<?php

/**
 * $Id: Site.php 85 2010-07-07 17:42:43Z bajt $
 * ----------------------------------------------
 * Config class
 *
 * @package RRsoft-CMS * @version $Rev: 30 $
 * @copyright (c) 2009-2010 RRsoft www.rrsoft.cz
 * @license GNU Public License
 *
 * Minimum Requirement: PHP 5.1.x
 */

namespace Wbengine;

abstract class Config {

    /**
     * Default adapter name
     */
    CONST ADAPTER_TYPE_ARRAY = "Array";


    /**
     * Config file type
     * @var string
     */
    CONST ADAPTER_TYPE_XML = "Xml";


    /**
     * Config file type
     * @var string
     */
    CONST ADAPTER_TYPE_YAML = "Yaml";


    /**
     * Config file type
     * @var string
     */
    CONST ADAPTER_TYPE_JASON = "Json";



    /**
     * Path to config file
     * @var string
     */
    private static $configFilePath = NULL;


    /**
     * Config adapter type
     * @var string
     */
    private static $configAdapter = NULL;


    /**
     * Config adapter instance
     * @var Config\Adapter\ConfigAdapterInterface
     */
    private static $adapter = NULL;


    /**
     * File extension
     * @var string
     */
    private static $fileExtension = null;


    /**
     * This method ceate config adapter by
     * given adapter type.
     *
     * @see local const
     * @param string $filePath
     * @param string $adapterType
     * @throws Exception\RuntimeException
     * @return Config\Adapter\ConfigAdapterInterface
     */
    static function load( $filePath, $adapterType )
    {
	if ( is_null($filePath) ) {

	    throw new Exception\RuntimeException(__METHOD__ .
	    ': Config file path cannot be null!');
	}
	if ( is_null($adapterType) ) {

	    throw new Exception\RuntimeException(__METHOD__ .
	    ': Config adapter type cannot be null!');
	}

	self::$configFilePath = $filePath;
	self::$configAdapter = $adapterType;

	//@TODO - Define more adapters...
	self::setAdapter($filePath, $adapterType);
	return self::getAdapter();
    }


    /**
     * return stored config adapter
     * @return Config\Adapter\ConfigAdapterInterface
     */
    public static function getAdapter()
    {
	return self::$adapter;
    }


    /**
     * Return curent file extension
     * @return string
     */
    private static function createFileExtension( $adapterType )
    {
	if ( $adapterType === self::ADAPTER_TYPE_ARRAY ) {

	    return "." . substr(strrchr(__FILE__, '.'), 1);
	}
	return "." . $adapterType;
    }


    /**
     * Create and store config adapter object
     * instance by gien params...
     *
     * @param string $filePath
     * @param string $adapterType
     * @throws Exception\RuntimeException
     */
    private static function setAdapter( $filePath, $adapterType )
    {
	$fileName = dirname(__DIR__)
		. "/wbengine/Config/Adapter/AdapterConfig"
		. ucfirst($adapterType)
		. self::createFileExtension($adapterType);
//	die($fileName);
	if ( !is_readable($fileName) ) {
	    throw new Exception\RuntimeException(__METHOD__ .
	    ': Adapter file ' . $fileName . ' does not exist!');
	}

	$className = self::createClassName($adapterType);

	switch ($adapterType) {
	    case self::ADAPTER_TYPE_ARRAY:

		self::$adapter = (New $className(require $filePath, FALSE));

		break;

	    default:
		break;
	}
    }


    /**
     * Create adapter class name
     * @param string $name
     * @return string
     * @throws Exception\RuntimeException
     */
    private static function createClassName( $name )
    {
	$className = 'Wbengine\Config\Adapter\AdapterConfig' . $name;

	if ( !class_exists($className) ) {
	    throw new Exception\RuntimeException(__METHOD__ .
	    ': Adapter class ' . $className . ' does not exist!');
	}
	return (string) $className;
    }

}
