<?php

/**
 * @see Class_Exception
 */

namespace Wbengine\Db\Exception;

use Wbengine\Exception;

class DbException extends Exception\RuntimeException {

}
