<?php

/**
 * Exception
 */

namespace Wbengine\Box\Exception;

use Wbengine\Exception;
use Wbengine\Exception\RuntimeException;

class BoxException extends RuntimeException {

}
